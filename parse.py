#!/usr/bin/python3

import csv, numpy, sys, wget, os, calendar, time
import matplotlib.pyplot as plt

CSV_FILE_NAME = './covid19-download.csv'
CSV_URL = 'https://health-infobase.canada.ca/src/data/covidLive/covid19-download.csv'

class DataPoint:
    def __init__(self, area, date, day, conf, deaths, total, totald, tests, totalt):
        self.area = area
        self.date = date
        # a lot of integer fields are stored as x.0, ie. 10.0 confirmed cases
        self.conf = int(float(conf))
        self.deaths = int(float(deaths))
        self.total = int(float(total))
        self.totald = int(float(totald))
        self.tests = int(float(tests if tests != '' else 0))
        self.totalt = int(float(totalt if totalt != '' else 0))

def n_range(element, n):
    return range(max(0, element-n), min(len(data_points), element+n+1))

def get_percent_increase(i, attr):
    global data_points
    if i - 1 < 0 or getattr(data_points[i - 1], attr) == 0 or i > len(data_points):
        return 1.0
    return getattr(data_points[i], attr) / getattr(data_points[i - 1], attr)

def n_day_increase_average(element, attr, n):
    global data_points
    ntotal = 0
    total = 0
    for i in n_range(element, n):
        ntotal += 1
        total += get_percent_increase(i, attr)
    return total / ntotal

def n_day_average(element, attr, n):
    global data_points
    ntotal = 0
    total = 0
    for i in n_range(element, n):
        ntotal += 1
        total += getattr(data_points[i], attr)
    return total / ntotal

if os.path.exists(CSV_FILE_NAME) and (os.path.getmtime(CSV_FILE_NAME) + 86400) < calendar.timegm(time.gmtime()):
    os.remove(CSV_FILE_NAME)

if (not os.path.exists(CSV_FILE_NAME)):
    print('Downloading new CSV file')
    wget.download(CSV_URL, CSV_FILE_NAME)
    print()

data_points = []
country_code = sys.argv[1]
smoothing = int(sys.argv[2])
dp_start = int(sys.argv[3])

with open('covid19-download.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            data_points.append(DataPoint("Start", 0, 0, 0, 0, 0, 0, 0 ,0))
        else:
            if row[0] != country_code:
                continue
            data_points.append(DataPoint(row[1], row[3], line_count, row[15], row[19], row[8], row[7], row[22], row[10]))
        
        line_count += 1

    print(f'Processed {line_count} lines.')

max_deaths = 0
max_deaths_date = ""
max_confirmed = 0
max_confirmed_date = ""
for dp in data_points:
    if dp.conf > max_confirmed:
       max_confirmed = dp.conf
       max_confirmed_date = dp.date

    if dp.deaths > max_deaths:
        max_deaths = dp.deaths
        max_deaths_date = dp.date

print(f'Max Confirmed: {max_confirmed} on {max_confirmed_date}')
print(f'Max Deaths: {max_deaths} on {max_deaths_date}')

for i in range(len(data_points) - 5, len(data_points)):
    print(f'Date: {data_points[i].date}, Confirmed: {data_points[i].conf}, Deaths: {data_points[i].deaths}')

degree = 20
dpset = range(dp_start, len(data_points))
fig, ax1 = plt.subplots()

x = numpy.array(dpset)
y = numpy.array([n_day_average(i, "conf", smoothing) for i in dpset])
ax1.plot(x, y)

z = numpy.polyfit(x, y, degree)
p = numpy.poly1d(z)
ax1.plot(x,p(x),"r--")

ax1.set_xlabel('Days since')
ax1.set_ylabel('Confirmed Cases')

ax2 = ax1.twinx()
# y = numpy.array([n_day_average(i, "conf", smoothing) / max(1, n_day_average(i, "tests", smoothing)) for i in dpset])
# y = numpy.array([n_day_average(i, "deaths", smoothing) / max(1, n_day_average(i, "conf", smoothing)) for i in dpset])
y = numpy.array([n_day_average(i, "deaths", smoothing) for i in dpset])
ax2.plot(x, y, color = "tab:orange")

z = numpy.polyfit(x, y, degree)
p = numpy.poly1d(z)
ax2.plot(x,p(x),"g--")
  
ax2.set_ylabel('Deaths')
  
# plt.axhline(y=1.0, color='r', linestyle='-')
  
# fig.tight_layout()
plt.title(f'{data_points[-1].area} Covid Smoothing:{smoothing}')
plt.show()
